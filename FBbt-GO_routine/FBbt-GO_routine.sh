#The file FBbt-GO.obo contains all terms from fbbt-simple.obo and all terms that are subclasses of GO:0005575 'cellular component' from GO. It is for use in canto to provide a list of valid anatomy terms for phenotype curation at FlyBase and is (intentionally) missing some classification on the GO terms. #Most of the header comes from fbbt-simple.obo.

#A new FBbt-GO.obo can be generated by running the following commands in a terminal after navigating to this location.
#You must have ROBOT installed - see http://robot.obolibrary.org/

## Copy the two ontologies to merge from ./import_export/ to ./FBbt-GO_routine/ 
cp ./import_export/go-basic.obo ./FBbt-GO_routine
cp ./import_export/fly_anatomy.obo ./FBbt-GO_routine

cd ./FBbt-GO_routine
## Extract everything that is a subclass of 'GO: cellular component'
robot extract --method MIREOT \
    --input go-basic.obo \
    --branch-from-term http://purl.obolibrary.org/obo/GO_0005575 \
    --output GO_cellcomponent_module_mireot.owl

## Get all labels in each ontology
robot query --input GO_cellcomponent_module_mireot.owl \
    --query get_labels.sparql GO_labels.csv
robot query --input fly_anatomy.obo \
    --query get_labels.sparql fbbt_labels.csv

## Make a list of fbbt IDs that have same labels as GO terms and remove these from fbbt-simple
python duplicate_label_id_finder.py
robot remove --input fly_anatomy.obo \
    --term-file duplicate_terms.txt \
    --select classes \
    --output fbbt-no-GO.obo

## Merge this module into fbbt-simple.obo and annotate with github location
robot merge --input fbbt-no-GO.obo \
    --input GO_cellcomponent_module_mireot.owl \
annotate --ontology-iri "https://github.com/FlyBase/drosophila-anatomy-developmental-ontology/for_canto/fbbt-GO.obo" \
    --output pre-FBbt-GO.obo

## Delete all lines "namespace: cellular_component"
grep -v "namespace: cellular_component"  pre-FBbt-GO.obo > FBbt-GO.obo

## copy

## Cleanup - Delete the two original ontologies, GO module, term lists and temp files
rm go-basic.obo fly_anatomy.obo GO_cellcomponent_module_mireot.owl GO_labels.csv fbbt_labels.csv duplicate_terms.txt fbbt-no-GO.obo pre-FBbt-GO.obo

cd ..

mv ./FBbt-GO_routine/FBbt-GO.obo ./import_export/