#!/bin/bash

#this script prints all the activity log messages from the running canto-server container into the file 'canto_running.log' in the 'logs' sub-folder of 'canto-space'

# Define canto-space root
CANTOSPACE="/data/export/canto-project/canto-space"

# Define logs folder inside canto-space
LOGS="logs"

# Define log file
LOGFILE="${CANTOSPACE}/${LOGS}/canto_server_running.log"

#this line retrieves the canto container id
DOCKERNAME=$(docker ps -a --filter status=running --filter name=^/canto$ | awk 'NR==2 { print $1 }')

	if [[ ! -z "${DOCKERNAME}" ]]; then
	echo "Canto running on container ${DOCKERNAME} - see canto_server_running.log for the log messages"
	
	# Redirect all output to logfile
	exec &> "${LOGFILE}"
	
	#docker logs is docker's command to retrieve all log messages from a specific container
	docker logs "${DOCKERNAME}"

	else echo "Canto not running - no log messages to show"
	
  fi

exit
