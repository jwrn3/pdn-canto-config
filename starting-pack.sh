#!/bin/sh

#before start, add the content of the folder 'starting-pack' to ./canto-space/
#run this @ /canto-space/

#create a 'logs' subfolder, where all log files will live
mkdir logs

#add canto_deploy.yaml (the configuration file) to ./canto-space/canto/
##comment for vitor - add username and password for chado @ deneb?
mv canto_deploy.yaml ./canto/


#enable server mode

##comment for James: make sure the CANTO-SPACE variable of canto-docker-initd has the correct filepath
##create a canto-docker-initd file @ /sbin/ and replace the one @ ./canto/etc/
##make them executable
cp canto-docker-initd /sbin/
chmod a+x /sbin/canto-docker-initd
cp canto-docker-initd ./canto/etc/
chmod a+x ./canto/etc/canto-docker-initd
rm canto-docker-initd

##create a 'canto' file @ /etc/init.d
##and make it executable
mv canto_for_etc-initd /etc/init.d/canto
chmod a+x /etc/init.d/canto


#enable memcached - for caching of the servers 
##comment for vitor: not sure it needs more stuff - ask Kim??
apt-get install memcached


#this loop creates cv terms for all the range of values for the 'priority curation score' within the internal canto database: 0-36
for i in {0..36}; do 
score="./canto/script/canto_docker ./script/canto_add.pl --cvterm \"Canto curation priorities\" "$i" FB:cantoscore"$i;
$score
done


this section will create CV terms for triage statuses, so that the publication list can be sub-divifed into more specific lists. This list is the current in use for phenotype curation (e.g. disease, pheno, training, etc).
./canto/script/canto_docker ./script/canto_add.pl --cvterm "Canto publication triage status" "DISEASE"
./canto/script/canto_docker ./script/canto_add.pl --cvterm "Canto publication triage status" "PHENO"
./canto/script/canto_docker ./script/canto_add.pl --cvterm "Canto publication triage status" "PHENO_ANAT"
./canto/script/canto_docker ./script/canto_add.pl --cvterm "Canto publication triage status" "PHENO_CHEM"
./canto/script/canto_docker ./script/canto_add.pl --cvterm "Canto publication triage status" "PHENO_DATASET"
./canto/script/canto_docker ./script/canto_add.pl --cvterm "Canto publication triage status" "LOW_PRIORITY"
./canto/script/canto_docker ./script/canto_add.pl --cvterm "Canto publication triage status" "TRAINING"
./canto/script/canto_docker ./script/canto_add.pl --cvterm "Canto publication triage status" "HIGH_PRIORITY"


#canfigure the taxon (Drosophila melanogaster)
./canto/script/canto_docker ./script/canto_add.pl --organism "Drosophila melanogaster" 7227 [fruit fly]


#add the 'mock' user "FlyBase Curator" (email address ignore@flybase.org), which will be used for publication data import
./canto/script/canto_docker ./script/canto_add.pl --person "FlyBase Curator" ignore@flybase.org pass '0000-0000-0000-0000' admin


#start canto
/etc/init.d/canto start


#run the weekly routine script, which will a) update and load ontologies (and configures rules for the usage of qualifer) and b) plug the most up-to-date database version into the configuration file canto_deploy.yaml
bash weekly_routine.sh


